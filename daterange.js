class DateRange extends HTMLElement {
    constructor() {
        super();

        const shadow = this.attachShadow({mode: 'open'});
        const self_body = document.createElement('div');

        self_body.innerHTML = `
            с <input type="date"/>
            по <input type="date"/>
            <br/>
            Разница: <span>0</span> д.
        `;

        this.__elem_begin = self_body.children[0];
        this.__elem_end = self_body.children[1];
        this.__elem_diff = self_body.children[3];

        const bound_updater = this.__update.bind(this);
        this.__elem_begin.addEventListener('change', bound_updater);
        this.__elem_end.addEventListener('change', bound_updater);

        shadow.appendChild(self_body);
    }

    __update() {
        if (!(this.begin && this.end))
            return;

        const begin = new Date(...this.begin.split('-'));
        const end = new Date(...this.end.split('-'));

        if (begin > end) {
            if (!this.on_wrong_range())
                return;
        }

        this.__elem_diff.innerHTML = Math.floor((end - begin) / 86400000);
    }

    on_wrong_range() {
        alert('Конечная дата не может быть < начальной!');
        this.__elem_begin.value = this.__elem_end.value = '';
        this.__elem_diff.innerHTML = '0';
        return false;
    }

    get begin() {
        return this.__elem_begin.value;
    }

    get end() {
        return this.__elem_end.value;
    }

    get diff() {
        return this.__elem_diff.innerHTML;
    }
}


customElements.define('date-range', DateRange);
